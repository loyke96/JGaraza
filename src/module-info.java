module org.unibl.etf.pj2.garage {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    exports org.unibl.etf.pj2.garage to javafx.graphics;
    opens org.unibl.etf.pj2.garage.admin to javafx.fxml;
    opens org.unibl.etf.pj2.garage.vehicles_management to javafx.fxml;
    opens org.unibl.etf.pj2.garage.simulation.window to javafx.fxml;
    opens org.unibl.etf.pj2.garage.vehicles to javafx.base;
}
