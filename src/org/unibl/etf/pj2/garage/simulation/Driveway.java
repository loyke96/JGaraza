package org.unibl.etf.pj2.garage.simulation;

import org.unibl.etf.pj2.garage.util.Storage;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class Driveway {
    public static boolean[] blocked = new boolean[Storage.getNumberOfPlatforms()];
    private volatile static int lookingForParking = 0;
    private static ConcurrentHashMap<Integer, String> driveway = new ConcurrentHashMap<>();
    private static Random random = new Random();
    private static boolean[] possibleCrash;

    static {
        possibleCrash = new boolean[Storage.getNumberOfPlatforms()];
        for (int i = 0; i < possibleCrash.length; i++) {
            possibleCrash[i] = true;
        }
    }

    static void occupy(int platform, int column, int row, String value, boolean rotation) throws InterruptedException {
        int key = platform * 100 + column * 10 + row;
        if (rotation) {
            if (!driveway.containsKey(key)) {
                driveway.put(key, value);
            }
            return;
        }
        while (blocked[platform - 1]) {
            Integer monitor = platform;
            synchronized (monitor) {
                monitor.wait();
            }
        }
        while (driveway.containsKey(key)) {
            String s = driveway.get(key);
            if (s != null) {
                if (possibleCrash[platform - 1] && row != 0 && random.nextDouble() < 0.1) {
                    possibleCrash[platform - 1] = false;
                    blocked[platform - 1] = true;
                    driveway.put(key, "X");
                    InvestigationVehicle.sendHelp(platform, column, row);
                    return;
                } else {
                    synchronized (s) {
                        s.wait();
                    }
                }
            }
        }
        driveway.put(key, value);
    }

    static void free(int platform, int column, int row, boolean rotation) {
        int key = platform * 100 + column * 10 + row;
        String value = driveway.get(key);
        if (rotation) {
            if (value != null && value.endsWith("R")) {
                driveway.remove(key);
                synchronized (value) {
                    value.notify();
                }
            }
        } else {
            driveway.remove(key);
            if (value != null) synchronized (value) {
                value.notify();
            }
        }
    }

    public static String get(int platform, int column, int row) {
        return driveway.get(platform * 100 + column * 10 + row);
    }

    synchronized static void addLookingForParking() {
        lookingForParking++;
    }

    synchronized static void removeLookingForParking() {
        lookingForParking--;
    }

    public static int getLookingForParking() {
        return lookingForParking;
    }
}
