package org.unibl.etf.pj2.garage.simulation;

import org.unibl.etf.pj2.garage.simulation.window.SimulationUtils;
import org.unibl.etf.pj2.garage.util.ErrorLogger;
import org.unibl.etf.pj2.garage.util.Payment;
import org.unibl.etf.pj2.garage.util.Storage;
import org.unibl.etf.pj2.garage.vehicles.Vehicle;

import java.util.logging.Level;

public class MovingVehicle extends Thread {
    private int platform;
    private int column;
    private int row;
    private Vehicle vehicle;
    private Direction direction;
    private String symbol;

    public MovingVehicle(int platform, int column, int row, Vehicle vehicle, Direction direction) {
        this.platform = platform;
        this.column = column;
        this.row = row;
        this.vehicle = vehicle;
        this.direction = direction;
        this.symbol = getSymbol(vehicle.getType());
    }

    @Override
    public void run() {
        try {
            if (direction == Direction.EXIT) {
                Storage.remove(vehicle, platform);
                exitGarage();
                Payment.charge(vehicle);
            } else if (direction == Direction.PARK) {
                Driveway.addLookingForParking();
                Payment.add(vehicle);
                findParkingSpot();
            }
        } catch (InterruptedException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    public static String getSymbol(String type) {
        if (type.contains("Police"))
            return "P";
        else if (type.contains("Medical"))
            return "H";
        else if (type.contains("Firefighters"))
            return "F";
        else
            return "V";
    }

    private void exitGarage() throws InterruptedException {
        ParkingSpots.free(platform, column, row);
        while (platform > 0) {
            int oldPlatform = platform;
            int oldColumn = column;
            int oldRow = row;
            if (row == 0) {
                if (column == 0) {
                    column = 7;
                    --platform;
                } else {
                    --column;
                }
            } else {
                switch (column) {
                    case 0:
                    case 1:
                    case 4:
                    case 5:
                        ++column;
                        break;
                    case 2:
                    case 6:
                        --row;
                        break;
                    case 3:
                    case 7:
                        --column;
                        break;
                }
            }
            if (platform > 0)
                Driveway.occupy(platform, column, row, symbol, false);
            Driveway.free(oldPlatform, oldColumn, oldRow, false);
            sleep(SimulationUtils.speed);
        }
    }

    private void findParkingSpot() throws InterruptedException {
        boolean end = false;
        while (!end) {
            int oldPlatform = platform;
            int oldColumn = column;
            int oldRow = row;
            if (row == 1) {
                if (column == 1) {
                    if (Storage.getVehiclesInPlatform(platform).size() < 28) {
                        Storage.save(vehicle, platform);
                        Driveway.removeLookingForParking();
                        ++row;
                    } else {
                        ++column;
                    }
                } else if (column == 7) {
                    ++platform;
                    column = 0;
                } else {
                    ++column;
                }
            } else {
                switch (column) {
                    case 0:
                    case 7:
                        end = true;
                        break;
                    case 1:
                        if (ParkingSpots.occupy(platform, column - 1, row, symbol)) {
                            --column;
                        } else if (ParkingSpots.occupy(platform, column + 2, row, symbol)) {
                            ++column;
                        } else {
                            if (row == 9) {
                                ++column;
                            } else {
                                ++row;
                            }
                        }
                        break;
                    case 2:
                        ++column;
                        break;
                    case 3:
                    case 4:
                        if (row > 1 && row < 8) {
                            end = true;
                        } else {
                            ++column;
                        }
                        break;
                    case 5:
                        if (row == 9) {
                            ++column;
                        } else {
                            --column;
                        }
                        break;
                    case 6:
                        if (ParkingSpots.occupy(platform, column + 1, row, symbol)) {
                            ++column;
                        } else if (ParkingSpots.occupy(platform, column - 2, row, symbol)) {
                            --column;
                        } else {
                            --row;
                        }
                }
            }
            if (!end)
                Driveway.occupy(platform, column, row, symbol, false);
            Driveway.free(oldPlatform, oldColumn, oldRow, false);
            sleep(SimulationUtils.speed);
        }
    }

    public enum Direction {
        EXIT, PARK
    }
}
