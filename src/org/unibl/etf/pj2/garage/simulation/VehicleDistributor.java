package org.unibl.etf.pj2.garage.simulation;

import org.unibl.etf.pj2.garage.util.Payment;
import org.unibl.etf.pj2.garage.util.RandomVehicle;
import org.unibl.etf.pj2.garage.util.Storage;
import org.unibl.etf.pj2.garage.vehicles.Vehicle;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Collectors;

public class VehicleDistributor {
    private static Random random = new Random();
    private static ArrayList<MovingVehicle> movingVehicles = new ArrayList<>();
    private static ArrayList<MovingVehicle> vehiclesToExit = null;

    public static void parkAll(int minimum) {
        int column, row;
        for (int i = 1; i <= Storage.getNumberOfPlatforms(); i++) {
            for (int j = Storage.getVehiclesInPlatform(i).size(); j < minimum; j++) {
                Vehicle vehicle = RandomVehicle.build();
                Payment.add(vehicle);
                Storage.save(vehicle, i);
            }
            for (Vehicle vehicle : Storage.getVehiclesInPlatform(i)) {
                String symbol = MovingVehicle.getSymbol(vehicle.getType());
                do {
                    column = random.nextInt(8);
                    row = random.nextInt(10);
                } while (!ParkingSpots.occupy(i, column, row, symbol));
                movingVehicles.add(new MovingVehicle(i, column, row, vehicle, MovingVehicle.Direction.EXIT));
            }
        }
    }

    public static ArrayList<MovingVehicle> getVehiclesForExit() {
        if (vehiclesToExit == null) {
            vehiclesToExit = movingVehicles.parallelStream().filter(movingVehicle -> random.nextDouble() < 0.15)
                    .collect(Collectors.toCollection(ArrayList::new));
        }
        return vehiclesToExit;
    }
}
