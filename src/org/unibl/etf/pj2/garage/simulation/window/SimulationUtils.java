package org.unibl.etf.pj2.garage.simulation.window;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Paint;
import org.unibl.etf.pj2.garage.simulation.Driveway;
import org.unibl.etf.pj2.garage.simulation.ParkingSpots;
import org.unibl.etf.pj2.garage.simulation.VehicleDistributor;
import org.unibl.etf.pj2.garage.util.ErrorLogger;

import java.util.ArrayList;
import java.util.logging.Level;

public class SimulationUtils {
    public static final int speed = 300;
    private static boolean end = false;
    private static Slider slider;
    private static ArrayList<Label> aquaLabels = new ArrayList<>();
    private static ArrayList<Label> roseLabels = new ArrayList<>();

    static void fillGridPane(GridPane gridPane) {
        Border border = new Border(new BorderStroke(Paint.valueOf("Gray"), BorderStrokeStyle.SOLID, null, BorderStroke.THIN));
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 10; j++) {
                final Label label = new Label();
                label.setPrefSize(30, 30);
                label.setAlignment(Pos.CENTER);
                label.setBorder(border);
                if (((i == 0 || i == 7) && j > 1) || (i == 3 || i == 4) && j > 1 && j < 8) {
                    label.setStyle("-fx-background-color: aqua");
                    aquaLabels.add(label);
                } else {
                    label.setStyle("-fx-background-color: mistyrose");
                    roseLabels.add(label);
                }
                gridPane.add(label, i, j);
            }
        }
    }

    private static void setAquaLabelText(Label label) {
        label.setText(ParkingSpots.get((int) slider.getValue(), GridPane.getColumnIndex(label), GridPane.getRowIndex(label)));
    }

    private static void setRoseLabelText(Label label) {
        label.setText(Driveway.get((int) slider.getValue(), GridPane.getColumnIndex(label), GridPane.getRowIndex(label)));
    }

    static void addSliderListener(Slider slider) {
        SimulationUtils.slider = slider;
        slider.valueProperty().addListener(observable -> {
            aquaLabels.forEach(SimulationUtils::setAquaLabelText);
            roseLabels.forEach(SimulationUtils::setRoseLabelText);
        });
    }

    static void start() {
        new Thread(() -> {
            while (!end) {
                Platform.runLater(() -> {
                    aquaLabels.forEach(SimulationUtils::setAquaLabelText);
                    roseLabels.forEach(SimulationUtils::setRoseLabelText);
                });
                try {
                    Thread.sleep(speed);
                } catch (InterruptedException e) {
                    ErrorLogger.log(Level.SEVERE, e.getMessage());
                }
            }
        }).start();
        VehicleDistributor.getVehiclesForExit().forEach(Thread::start);
    }

    public static void end() {
        end = true;
    }
}
