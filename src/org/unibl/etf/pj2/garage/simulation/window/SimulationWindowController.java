package org.unibl.etf.pj2.garage.simulation.window;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import org.unibl.etf.pj2.garage.simulation.Driveway;
import org.unibl.etf.pj2.garage.simulation.MovingVehicle;
import org.unibl.etf.pj2.garage.simulation.VehicleDistributor;
import org.unibl.etf.pj2.garage.util.Payment;
import org.unibl.etf.pj2.garage.util.RandomVehicle;
import org.unibl.etf.pj2.garage.util.Storage;

import java.util.Objects;
import java.util.stream.IntStream;

public class SimulationWindowController {
    @FXML
    private GridPane gridPane;
    @FXML
    private Button startButton;
    @FXML
    private Slider platformSlider;

    @FXML
    private void onExitButtonClick() {
        gridPane.getScene().getWindow().hide();
        SimulationUtils.end();
        Storage.serialize();
        Payment.serialize();
    }

    @FXML
    private void onStartButtonClick() {
        if (startButton.getText().equals("Start")) {
            VehicleDistributor.parkAll((int) platformSlider.getValue());
            platformSlider.setMax(Storage.getNumberOfPlatforms());
            platformSlider.setMin(1);
            SimulationUtils.fillGridPane(gridPane);
            SimulationUtils.addSliderListener(platformSlider);
            platformSlider.setValue(1);
            startButton.setText("Add vehicle");
            SimulationUtils.start();
        } else {
            int parkedVehicles = IntStream.rangeClosed(1, Storage.getNumberOfPlatforms()).map(i -> Storage.getVehiclesInPlatform(i).size()).sum();
            parkedVehicles += Driveway.getLookingForParking();
            if (parkedVehicles < Storage.getNumberOfPlatforms() * 28) {
                new MovingVehicle(0, 7, 1,
                        Objects.requireNonNull(RandomVehicle.build()), MovingVehicle.Direction.PARK).start();
            } else {
                startButton.setText("Garage is full");
            }
        }
    }
}
