package org.unibl.etf.pj2.garage.simulation;

import java.util.concurrent.ConcurrentHashMap;

public class ParkingSpots {
    private static ConcurrentHashMap<Integer, String> parkingSpot = new ConcurrentHashMap<>();

    private static boolean checkIfParkingSlot(int column, int row) {
        switch (column) {
            case 0:
            case 7:
                return row > 1;
            case 3:
            case 4:
                return row > 1 && row < 8;
            default:
                return false;
        }
    }

    public static boolean occupy(int platform, int column, int row, String value) {
        if (!checkIfParkingSlot(column, row)) {
            return false;
        }
        Integer key = platform * 100 + column * 10 + row;
        if (parkingSpot.containsKey(key)) {
            return false;
        } else {
            parkingSpot.put(key, value);
            return true;
        }
    }

    public static void free(int platform, int column, int row) {
        parkingSpot.remove(platform * 100 + column * 10 + row);
    }

    public static String get(int platform, int column, int row) {
        return parkingSpot.get(platform * 100 + column * 10 + row);
    }
}
