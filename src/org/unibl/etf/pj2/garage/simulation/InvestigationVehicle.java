package org.unibl.etf.pj2.garage.simulation;

import org.unibl.etf.pj2.garage.simulation.window.SimulationUtils;
import org.unibl.etf.pj2.garage.util.ErrorLogger;
import org.unibl.etf.pj2.garage.util.Evidence;
import org.unibl.etf.pj2.garage.util.RandomVehicle;
import org.unibl.etf.pj2.garage.vehicles.FirefightersVan;
import org.unibl.etf.pj2.garage.vehicles.MedicalCar;
import org.unibl.etf.pj2.garage.vehicles.PoliceCar;

import java.util.Random;
import java.util.logging.Level;

public class InvestigationVehicle extends Thread {
    private static Random random = new Random();
    private int platform;
    private int column;
    private int row;
    private int crashPlatform;
    private int crashColumn;
    private int crashRow;
    private String symbol;

    private InvestigationVehicle(int platform, int column, int row, String type) {
        this.platform = 0;
        this.column = 7;
        this.row = 1;
        this.crashPlatform = platform;
        this.crashColumn = column;
        this.crashRow = row;
        this.symbol = type + "R";
    }

    public static void sendHelp(int platform, int column, int row) {
        try {
            new InvestigationVehicle(platform, column, row, "H").start();
            Thread.sleep(SimulationUtils.speed * 2);
            new InvestigationVehicle(platform, column, row, "F").start();
            Thread.sleep(SimulationUtils.speed * 2);
            new InvestigationVehicle(platform, column, row, "P").start();
        } catch (Throwable t) {
            ErrorLogger.log(Level.SEVERE, t.getMessage());
        }
    }

    @Override
    public void run() {
        try {
            boolean end = false;
            while (!end) {
                int oldPlatform = platform;
                int oldColumn = column;
                int oldRow = row;
                int absRow = Math.abs(row - crashRow);
                int absColumn = Math.abs(column - crashColumn);
                if (absColumn + absRow < 2) {
                    end = true;
                } else if (row == 1) {
                    if (column == 1) {
                        if (platform == crashPlatform) {
                            ++row;
                        } else {
                            ++column;
                        }
                    } else if (column == 7) {
                        ++platform;
                        column = 0;
                    } else {
                        ++column;
                    }
                } else {
                    switch (column) {
                        case 1:
                            if (row == 9) {
                                ++column;
                            } else {
                                ++row;
                            }
                            break;
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            ++column;
                            break;
                        case 6:
                            --row;
                            break;
                    }
                }
                if (!end) {
                    Driveway.occupy(platform, column, row, symbol, true);
                }
                Driveway.free(oldPlatform, oldColumn, oldRow, true);
                sleep(SimulationUtils.speed);
            }
            Thread.sleep(random.nextInt(7_000) + 3_000);
            switch (symbol) {
                case "PR":
                    Integer monitor = crashPlatform;
                    Driveway.blocked[crashPlatform - 1] = false;
                    synchronized (monitor) {
                        monitor.notifyAll();
                    }
                    Evidence.accident(RandomVehicle.build(), RandomVehicle.build());
                    new MovingVehicle(crashPlatform, crashColumn, crashRow, new PoliceCar("", "",
                            "", "", 4, null), MovingVehicle.Direction.EXIT).start();
                    break;
                case "HR":
                    new MovingVehicle(crashPlatform, crashColumn, crashRow, new MedicalCar("", "",
                            "", "", 2, null), MovingVehicle.Direction.EXIT).start();
                    break;
                case "FR":
                    new MovingVehicle(crashPlatform, crashColumn, crashRow, new FirefightersVan("", "",
                            "", "", 500, null), MovingVehicle.Direction.EXIT).start();
                    break;
            }
        } catch (Throwable t) {
            t.printStackTrace();
            ErrorLogger.log(Level.SEVERE, t.getMessage());
        }
    }
}
