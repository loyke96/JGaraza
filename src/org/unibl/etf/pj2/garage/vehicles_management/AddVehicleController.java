package org.unibl.etf.pj2.garage.vehicles_management;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.unibl.etf.pj2.garage.util.DynamicFields;
import org.unibl.etf.pj2.garage.util.ImageSelector;
import org.unibl.etf.pj2.garage.util.VehicleSaver;
import org.unibl.etf.pj2.garage.vehicles.Vehicle;

import java.net.URL;
import java.util.ResourceBundle;

public class AddVehicleController implements Initializable {

    @FXML
    private ChoiceBox<String> vehicleTypeChoiceBox;
    @FXML
    private TextField titleTextField;
    @FXML
    private TextField chassisNumberTextField;
    @FXML
    private TextField engineNumberTextField;
    @FXML
    private TextField registrationNumberTextField;
    @FXML
    private ImageView vehicleImageView;
    @FXML
    private Label multipurposeLabel;
    @FXML
    private TextField multipurposeTextField;

    @FXML
    private void onPictureLabelClick() {
        final Image image = ImageSelector.select(multipurposeLabel.getScene().getWindow());
        vehicleImageView.setImage(image);
    }

    @FXML
    private void onCancelButtonClick() {
        multipurposeLabel.getScene().getWindow().hide();
        DynamicFields.setVehicleToEdit(null);
        clear();
    }

    @FXML
    private void onOKButtonClick() {
        Vehicle vehicle = DynamicFields.vehicleToEdit.getValue();
        try {
            VehicleSaver.checkInfo(vehicleTypeChoiceBox.getValue(), titleTextField.getText(), chassisNumberTextField.getText(),
                    engineNumberTextField.getText(), registrationNumberTextField.getText(),
                    vehicleImageView.getImage(), multipurposeTextField.getText());
            if (vehicle == null) {
                VehicleSaver.save(vehicleTypeChoiceBox.getValue(), titleTextField.getText(), chassisNumberTextField.getText(),
                        engineNumberTextField.getText(), registrationNumberTextField.getText(),
                        vehicleImageView.getImage(), multipurposeTextField.getText());
            } else {
                vehicle.setType(vehicleTypeChoiceBox.getValue());
                vehicle.setTitle(titleTextField.getText());
                vehicle.setChassisNumber(chassisNumberTextField.getText());
                vehicle.setEngineNumber(engineNumberTextField.getText());
                vehicle.setRegistrationNumber(registrationNumberTextField.getText());
                vehicle.getImage().setImage(vehicleImageView.getImage());
                vehicle.setNumberOfDoors(null);
                vehicle.setPermittedLoad(null);
                if (vehicle.getType().endsWith("Car")) {
                    vehicle.setNumberOfDoors(Integer.valueOf(multipurposeTextField.getText()));
                }
                if (vehicle.getType().endsWith("Van")) {
                    vehicle.setPermittedLoad(Integer.valueOf(multipurposeTextField.getText()));
                }
                DynamicFields.setVehicleToEdit(null);
            }
            multipurposeLabel.getScene().getWindow().hide();
            clear();
        } catch (
                Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Invalid input");
            alert.setHeaderText("Please check your input");
            alert.showAndWait();
        }
    }

    private void clear() {
        vehicleImageView.setImage(null);
        titleTextField.clear();
        chassisNumberTextField.clear();
        engineNumberTextField.clear();
        registrationNumberTextField.clear();
        multipurposeTextField.clear();
    }

    private void fill(Vehicle vehicle) {
        vehicleTypeChoiceBox.setValue(vehicle.getType());
        titleTextField.setText(vehicle.getTitle());
        chassisNumberTextField.setText(vehicle.getChassisNumber());
        engineNumberTextField.setText(vehicle.getEngineNumber());
        registrationNumberTextField.setText(vehicle.getRegistrationNumber());
        vehicleImageView.setImage(vehicle.getImage().getImage());
        if (vehicle.getType().endsWith("Van")) {
            multipurposeTextField.setText(vehicle.getPermittedLoad().toString());
        }
        if (vehicle.getType().endsWith("Car")) {
            multipurposeTextField.setText(vehicle.getNumberOfDoors().toString());
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        vehicleTypeChoiceBox.getItems().addAll("Car", "FirefightersVan", "MedicalCar", "MedicalVan",
                "Motorcycle", "PoliceCar", "PoliceMotorcycle", "PoliceVan", "Van");
        vehicleTypeChoiceBox.valueProperty().addListener((observable, oldValue, newValue) ->
                DynamicFields.loadInputFields(multipurposeLabel, multipurposeTextField, newValue));
        vehicleTypeChoiceBox.getSelectionModel().select(0);
        DynamicFields.vehicleToEdit.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                fill(newValue);
                vehicleTypeChoiceBox.setDisable(true);
            } else {
                vehicleTypeChoiceBox.setDisable(false);
                DynamicFields.getPlatformTableView().refresh();
            }
        });
    }
}
