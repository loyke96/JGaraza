package org.unibl.etf.pj2.garage.admin;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.unibl.etf.pj2.garage.util.DynamicFields;
import org.unibl.etf.pj2.garage.util.Payment;
import org.unibl.etf.pj2.garage.util.StagePool;
import org.unibl.etf.pj2.garage.util.Storage;
import org.unibl.etf.pj2.garage.vehicles.Vehicle;

import java.net.URL;
import java.util.ResourceBundle;

public class AdminController implements Initializable {
    @FXML
    private TableView<Vehicle> vehiclesTableView;
    @FXML
    private ChoiceBox<Integer> platformChoiceBox;

    @FXML
    private void onAddVehicleButtonClick() {
        if (vehiclesTableView.getItems().size() < 28) {
            StagePool.getAddVehicleStage().showAndWait();
        } else {
            final Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("Platform is full");
            alert.showAndWait();
        }
    }

    @FXML
    private void onDoubleClick(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2) {
            Vehicle item = vehiclesTableView.getSelectionModel().getSelectedItem();
            if (item != null) {
                StagePool.getAddVehicleStage().show();
                DynamicFields.setVehicleToEdit(item);
            }
        }
    }

    @FXML
    private void onStartSimulationButtonClick() {
        StagePool.getSimulationStage().show();
        vehiclesTableView.getScene().getWindow().hide();
    }

    @FXML
    private void onExitButtonClick() {
        vehiclesTableView.getScene().getWindow().hide();
        Storage.serialize();
        Payment.serialize();
    }

    @FXML
    private void onRemoveVehicleButtonClick() {
        final Vehicle item = vehiclesTableView.getSelectionModel().getSelectedItem();
        if (item != null) {
            DynamicFields.deleteAndUpdate(item);
            Payment.charge(item);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DynamicFields.setupPlatformTableView(vehiclesTableView);
        DynamicFields.fillChoiceBox(platformChoiceBox);
    }
}
