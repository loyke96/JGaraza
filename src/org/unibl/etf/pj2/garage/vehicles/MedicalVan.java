package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

public class MedicalVan extends Van implements InstitutionVehicle {

    public MedicalVan(String title, String chassisNumber, String engineNumber, String registrationNumber, Integer permittedLoad, SerializableImage image) {
        super("MedicalVan", title, chassisNumber, engineNumber, registrationNumber, permittedLoad, image);
    }
}
