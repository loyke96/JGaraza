package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

public class MedicalCar extends Car implements InstitutionVehicle {

    public MedicalCar(String title, String chassisNumber, String engineNumber, String registrationNumber, Integer numberOfDoors, SerializableImage image) {
        super("MedicalCar", title, chassisNumber, engineNumber, registrationNumber, numberOfDoors, image);
    }
}
