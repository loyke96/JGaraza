package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

public class PoliceCar extends PoliceVehicle {

    public PoliceCar(String title, String chassisNumber, String engineNumber, String registrationNumber, Integer numberOfDoors, SerializableImage image) {
        super("PoliceCar", title, chassisNumber, engineNumber, registrationNumber, numberOfDoors, null, image);
    }
}
