package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

public class PoliceVan extends PoliceVehicle {

    public PoliceVan(String title, String chassisNumber, String engineNumber, String registrationNumber, Integer permittedLoad, SerializableImage image) {
        super("PoliceVan", title, chassisNumber, engineNumber, registrationNumber, null, permittedLoad, image);
    }
}
