package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

public class Motorcycle extends Vehicle {

    public Motorcycle(String title, String chassisNumber, String engineNumber, String registrationNumber, SerializableImage image) {
        super("Motorcycle", title, chassisNumber, engineNumber, registrationNumber, null, null, image);
    }
}
