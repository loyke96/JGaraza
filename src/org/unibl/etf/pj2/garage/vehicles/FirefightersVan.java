package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

public class FirefightersVan extends Van implements InstitutionVehicle {

    public FirefightersVan(String title, String chassisNumber, String engineNumber, String registrationNumber, Integer permittedLoad, SerializableImage image) {
        super("FirefightersVan", title, chassisNumber, engineNumber, registrationNumber, permittedLoad, image);
    }
}
