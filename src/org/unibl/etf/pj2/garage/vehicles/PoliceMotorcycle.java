package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

public class PoliceMotorcycle extends PoliceVehicle {

    public PoliceMotorcycle(String title, String chassisNumber, String engineNumber, String registrationNumber, SerializableImage image) {
        super("PoliceMotorcycle", title, chassisNumber, engineNumber, registrationNumber, null, null, image);
    }
}
