package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class PoliceVehicle extends Vehicle implements InstitutionVehicle {
    public static final List<String> wanted;

    static {
        List<String> list = null;
        try {
            list = Files.readAllLines(Paths.get("resources/wanted.txt"));
        } catch (IOException e) {
            Logger.getLogger("error").log(Level.CONFIG, e.getMessage());
            Logger.getLogger("error").log(Level.WARNING, "Wanted file not found...");
        }
        wanted = list;
    }

    protected PoliceVehicle(String type, String title, String chassisNumber, String engineNumber, String registrationNumber, Integer numberOfDoors, Integer permittedLoad, SerializableImage image) {
        super(type, title, chassisNumber, engineNumber, registrationNumber, numberOfDoors, permittedLoad, image);
    }
}
