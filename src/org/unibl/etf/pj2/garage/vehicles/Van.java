package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

public class Van extends Vehicle {


    public Van(String title, String chassisNumber, String engineNumber, String registrationNumber, Integer permittedLoad, SerializableImage image) {
        super("Van", title, chassisNumber, engineNumber, registrationNumber, null, permittedLoad, image);
    }

    protected Van(String type, String title, String chassisNumber, String engineNumber, String registrationNumber, Integer permittedLoad, SerializableImage image) {
        super(type, title, chassisNumber, engineNumber, registrationNumber, null, permittedLoad, image);
    }
}
