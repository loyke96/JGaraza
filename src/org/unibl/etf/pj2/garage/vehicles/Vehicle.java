package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

import java.io.Serializable;

public abstract class Vehicle implements Serializable {
    private String type;
    private String title;
    private String chassisNumber;
    private String engineNumber;
    private String registrationNumber;
    private Integer numberOfDoors;
    private Integer permittedLoad;
    private SerializableImage image;

    protected Vehicle(String type, String title, String chassisNumber, String engineNumber, String registrationNumber, Integer numberOfDoors, Integer permittedLoad, SerializableImage image) {
        this.type = type;
        this.title = title;
        this.chassisNumber = chassisNumber;
        this.engineNumber = engineNumber;
        this.registrationNumber = registrationNumber;
        this.numberOfDoors = numberOfDoors;
        this.permittedLoad = permittedLoad;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public SerializableImage getImage() {
        return image;
    }

    public Integer getPermittedLoad() {
        return permittedLoad;
    }

    public void setPermittedLoad(Integer permittedLoad) {
        this.permittedLoad = permittedLoad;
    }

    public Integer getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(Integer numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
