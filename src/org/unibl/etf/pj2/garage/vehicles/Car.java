package org.unibl.etf.pj2.garage.vehicles;

import org.unibl.etf.pj2.garage.util.SerializableImage;

public class Car extends Vehicle {

    public Car(String title, String chassisNumber, String engineNumber, String registrationNumber, Integer numberOfDoors, SerializableImage image) {
        super("Car", title, chassisNumber, engineNumber, registrationNumber, numberOfDoors, null, image);
    }

    protected Car(String type, String title, String chassisNumber, String engineNumber, String registrationNumber, Integer numberOfDoors, SerializableImage image) {
        super(type, title, chassisNumber, engineNumber, registrationNumber, numberOfDoors, null, image);
    }
}
