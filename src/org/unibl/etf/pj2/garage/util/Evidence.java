package org.unibl.etf.pj2.garage.util;

import org.unibl.etf.pj2.garage.vehicles.Vehicle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.Clock;
import java.util.logging.Level;

public class Evidence {
    static private File userHome;

    static {
        userHome = new File(System.getProperty("user.home") + File.separatorChar + ".jgaraza");
        if (!userHome.exists()) {
            userHome.mkdir();
        }
    }

    public static void accident(Vehicle vehicle1, Vehicle vehicle2) {
        String fileName = userHome.getAbsolutePath() + File.separatorChar +
                "accident_" + System.currentTimeMillis() + ".bin";
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(fileName))) {
            outputStream.writeObject(Clock.systemDefaultZone());
            outputStream.writeObject(vehicle1);
            outputStream.writeObject(vehicle2);
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, "Cannot write to file " + fileName);
        }
    }

    public static void wantedVehicle(Vehicle vehicle) {
        String fileName = userHome.getAbsolutePath() + File.separatorChar +
                "wanted_" + System.currentTimeMillis() + ".bin";
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(fileName))) {
            outputStream.writeObject(Clock.systemDefaultZone());
            outputStream.writeObject(vehicle);
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, "Cannot write to file " + fileName);
        }
    }
}
