package org.unibl.etf.pj2.garage.util;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.unibl.etf.pj2.garage.vehicles.Vehicle;

import java.util.logging.Level;

public class DynamicFields {
    private static int currentPlatform = 1;
    private static TableView<Vehicle> platformTableView;
    public static Property<Vehicle> vehicleToEdit = new SimpleObjectProperty<>(null);

    public static void loadInputFields(Label label, TextField textField, String vehicleType) {
        textField.clear();
        if (vehicleType.endsWith("Car")) {
            label.setText("Number of doors");
            label.setVisible(true);
            textField.setVisible(true);
        } else if (vehicleType.endsWith("Motorcycle")) {
            textField.setVisible(false);
            label.setVisible(false);
        } else if (vehicleType.endsWith("Van")) {
            label.setText("Permitted load");
            label.setVisible(true);
            textField.setVisible(true);
        } else {
            ErrorLogger.log(Level.SEVERE, "Unrecognised vehicle type...");
            System.exit(1);
        }
    }

    static void saveAndUpdate(Vehicle vehicle) {
        platformTableView.getItems().add(vehicle);
        Storage.save(vehicle, currentPlatform);
    }

    public static void deleteAndUpdate(Vehicle vehicle) {
        platformTableView.getItems().remove(vehicle);
        Storage.remove(vehicle, currentPlatform);
    }

    private static void fillVehicleTable() {
        platformTableView.getItems().clear();
        platformTableView.getItems().addAll(Storage.getVehiclesInPlatform(currentPlatform));
    }

    public static void fillChoiceBox(ChoiceBox<Integer> choiceBox) {
        int platforms = Storage.getNumberOfPlatforms();
        for (int i = 1; i <= platforms; i++) {
            choiceBox.getItems().add(i);
        }
        choiceBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            DynamicFields.currentPlatform = newValue;
            fillVehicleTable();
        });
        choiceBox.setValue(currentPlatform);
    }

    public static TableView<Vehicle> getPlatformTableView() {
        return platformTableView;
    }

    public static void setupPlatformTableView(TableView<Vehicle> platformTableView) {
        DynamicFields.platformTableView = platformTableView;
        final ObservableList<TableColumn<Vehicle, ?>> columns = platformTableView.getColumns();
        columns.get(0).setCellValueFactory(new PropertyValueFactory<>("type"));
        columns.get(1).setCellValueFactory(new PropertyValueFactory<>("title"));
        columns.get(2).setCellValueFactory(new PropertyValueFactory<>("chassisNumber"));
        columns.get(3).setCellValueFactory(new PropertyValueFactory<>("engineNumber"));
        columns.get(4).setCellValueFactory(new PropertyValueFactory<>("registrationNumber"));
        columns.get(5).setCellValueFactory(new PropertyValueFactory<>("numberOfDoors"));
        columns.get(6).setCellValueFactory(new PropertyValueFactory<>("permittedLoad"));
    }

    public static void setVehicleToEdit(Vehicle vehicleToEdit) {
        DynamicFields.vehicleToEdit.setValue(vehicleToEdit);
    }
}
