package org.unibl.etf.pj2.garage.util;

import org.unibl.etf.pj2.garage.vehicles.Vehicle;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;

public class Storage {
    private static final ArrayList<Vehicle>[] vehiclesInPlatform;

    static {
        int platforms = PropertiesReader.getNumberOfPlatforms();
        ArrayList<Vehicle>[] tmp;
        vehiclesInPlatform = new ArrayList[platforms];
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("garage_parking.ser"))) {
            tmp = (ArrayList<Vehicle>[]) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            tmp = new ArrayList[platforms];
            for (int i = 0; i < platforms; i++) {
                tmp[i] = new ArrayList<>();
            }
        }
        for (int i = 0; i < tmp.length && i < platforms; i++) {
            vehiclesInPlatform[i] = tmp[i];
        }
        for (int i = tmp.length; i < platforms; i++) {
            vehiclesInPlatform[i] = new ArrayList<>();
        }
    }

    public static void save(Vehicle vehicle, int platform) {
        vehiclesInPlatform[platform - 1].add(vehicle);
    }

    public static void remove(Vehicle vehicle, int platform) {
        vehiclesInPlatform[platform - 1].remove(vehicle);
    }

    public static void serialize() {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("garage_parking.ser", false))) {
            objectOutputStream.writeObject(vehiclesInPlatform);
        } catch (IOException e) {
            ErrorLogger.log(Level.WARNING, "Cannot open garage_parking.ser for writing...");
        }
    }

    public static int getNumberOfPlatforms() {
        return vehiclesInPlatform.length;
    }

    public static ArrayList<Vehicle> getVehiclesInPlatform(int platform) {
        return vehiclesInPlatform[platform - 1];
    }
}
