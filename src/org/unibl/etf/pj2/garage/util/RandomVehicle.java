package org.unibl.etf.pj2.garage.util;

import javafx.scene.image.Image;
import org.unibl.etf.pj2.garage.vehicles.*;

import java.io.File;
import java.util.Random;

public class RandomVehicle {
    private static final Random random = new Random();

    private static final String symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private static String getRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(symbols.charAt(random.nextInt(symbols.length())));
        }
        return sb.toString();
    }

    private static Image getRandomImage() {
        File[] pictures = new File("pictures").listFiles((dir, name) ->
                name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".bmp"));
        return new Image(pictures[random.nextInt(pictures.length)].toURI().toString());
    }

    public static Vehicle build() {
        String title = getRandomString(5);
        String chassis = getRandomString(17);
        String engine = getRandomString(15);
        String licencePlate = getRandomString(7);
        SerializableImage img = new SerializableImage(getRandomImage());
        int load = random.nextInt(50) * 50 + 300;
        int doors = random.nextInt(4) + 2;
        switch (random.nextInt(60)) {
            case 0:
                return new PoliceCar(title, chassis, engine, licencePlate,
                        doors, img);
            case 1:
                return new PoliceMotorcycle(title, chassis, engine, licencePlate, img);
            case 2:
                return new PoliceVan(title, chassis, engine, licencePlate, load, img);
            case 3:
                return new MedicalCar(title, chassis, engine, licencePlate, doors, img);
            case 4:
                return new MedicalVan(title, chassis, engine, licencePlate, load, img);
            case 5:
                return new FirefightersVan(title, chassis, engine, licencePlate, load, img);
            default:
                switch (random.nextInt(3)) {
                    case 0:
                        return new Car(title, chassis, engine, licencePlate, doors, img);
                    case 1:
                        return new Motorcycle(title, chassis, engine, licencePlate, img);
                    case 2:
                        return new Van(title, chassis, engine, licencePlate, load, img);
                }
        }
        return null;
    }
}
