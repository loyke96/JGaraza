package org.unibl.etf.pj2.garage.util;

import org.unibl.etf.pj2.garage.vehicles.InstitutionVehicle;
import org.unibl.etf.pj2.garage.vehicles.Vehicle;

import java.io.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

public class Payment {
    private static ConcurrentHashMap<String, Long> timeWhenVehicleEntered;
    private static PrintWriter printWriter;

    static {
        try {
            printWriter = new PrintWriter(new FileOutputStream("payments.csv", true));
        } catch (FileNotFoundException e) {
            ErrorLogger.log(Level.SEVERE, e.getMessage());
            System.exit(2);
        }
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("garage_payment.ser"))) {
            timeWhenVehicleEntered = (ConcurrentHashMap<String, Long>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            timeWhenVehicleEntered = new ConcurrentHashMap<>();
        }
    }

    public static void add(Vehicle vehicle) {
        if (!(vehicle instanceof InstitutionVehicle)) {
            timeWhenVehicleEntered.put(vehicle.getRegistrationNumber(), System.currentTimeMillis());
        }
    }

    public static int charge(Vehicle vehicle) {
        int charge = 0;
        if (!(vehicle instanceof InstitutionVehicle)) {
            long elapsed = System.currentTimeMillis() - timeWhenVehicleEntered.remove(vehicle.getRegistrationNumber());
            while (elapsed > 86400000) {
                elapsed -= 86400000;
                charge += 8;
            }
            if (elapsed < 3600000) {
                charge += 1;
            } else if (elapsed < 10800000) {
                charge += 2;
            } else {
                charge += 8;
            }
            printWriter.println(vehicle.getRegistrationNumber() + "," + charge + ".00 KM");
        }
        return charge;
    }

    public static void serialize() {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("garage_payment.ser", false))) {
            objectOutputStream.writeObject(timeWhenVehicleEntered);
            printWriter.close();
        } catch (IOException e) {
            ErrorLogger.log(Level.WARNING, "Cannot open garage_payment.ser for writing...");
        }
    }
}
