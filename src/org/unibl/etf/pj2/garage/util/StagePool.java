package org.unibl.etf.pj2.garage.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.logging.Level;

public class StagePool {
    private static Stage addVehicleStage = null;
    private static Stage simulationStage = null;

    private static Stage loadFXML(String fxmlResource) {
        try {
            final Parent root = FXMLLoader.load(StagePool.class.getResource(fxmlResource));
            Stage stage = new Stage(StageStyle.UNDECORATED);
            stage.setScene(new Scene(root));
            stage.sizeToScene();
            stage.setResizable(false);
            stage.centerOnScreen();
            return stage;
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, "Resource " + fxmlResource + "not found...");
            System.exit(1);
        }
        return null;
    }

    public static Stage getAddVehicleStage() {
        if (addVehicleStage == null) {
            addVehicleStage = loadFXML("/org/unibl/etf/pj2/garage/vehicles_management/AddVehicleForm.fxml");
        }
        return addVehicleStage;
    }

    public static Stage getSimulationStage() {
        if (simulationStage == null) {
            simulationStage = loadFXML("/org/unibl/etf/pj2/garage/simulation/window/SimulationWindow.fxml");
        }
        return simulationStage;
    }
}
