package org.unibl.etf.pj2.garage.util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ErrorLogger {
    private static Logger logger = Logger.getLogger("error");

    public static void init() {
        try {
            logger.addHandler(new FileHandler("error.log", true));
            logger.setLevel(Level.INFO);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static void log(Level level, String message) {
        logger.log(level, message);
    }
}
