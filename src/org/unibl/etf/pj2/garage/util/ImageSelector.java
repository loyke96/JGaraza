package org.unibl.etf.pj2.garage.util;

import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

import java.io.File;

public class ImageSelector {
    static private final FileChooser fileChooser;

    static {
        fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("pictures"));
        fileChooser.getExtensionFilters().add(new ExtensionFilter(
                "Images", "*.jpg", "*.bmp", "*.png"));
    }

    public static Image select(Window parent) {

        final File imageFile = fileChooser.showOpenDialog(parent);
        if (imageFile == null) return null;
        return new Image(imageFile.toURI().toString());
    }
}
