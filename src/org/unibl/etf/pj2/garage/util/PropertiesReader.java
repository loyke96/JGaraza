package org.unibl.etf.pj2.garage.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;

public class PropertiesReader {
    public static int getNumberOfPlatforms() {
        String number = null;
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("resources/config.properties"));
            number = prop.getProperty("NumberOfPlatforms");
        } catch (IOException e) {
            ErrorLogger.log(Level.SEVERE, "Config file not found...");
            System.exit(1);
        }
        return Integer.valueOf(number);
    }
}
