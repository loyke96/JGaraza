package org.unibl.etf.pj2.garage.util;

import javafx.scene.image.Image;
import org.unibl.etf.pj2.garage.vehicles.Vehicle;

import java.util.InvalidPropertiesFormatException;

public class VehicleSaver {
    public static void save(String vehicleType, String title, String chassisNumber, String engineNumber,
                            String registrationNumber, Image image, String number) throws Exception {
        final SerializableImage image1 = new SerializableImage(image);
        Class<?> aClass = Class.forName("org.unibl.etf.pj2.garage.vehicles." + vehicleType);
        Vehicle vehicle;
        if (aClass.getName().contains("Motorcycle")) {
            vehicle = (Vehicle) aClass.getConstructors()[0]
                    .newInstance(title, chassisNumber, engineNumber, registrationNumber, image1);
        } else {
            vehicle = (Vehicle) aClass.getConstructors()[0]
                    .newInstance(title, chassisNumber, engineNumber, registrationNumber, Integer.valueOf(number), image1);
        }
        Payment.add(vehicle);
        DynamicFields.saveAndUpdate(vehicle);
    }

    public static void checkInfo(String vehicleType, String title, String chassisNumber, String engineNumber,
                                 String registrationNumber, Image image, String number) throws Exception {
        if (vehicleType.isEmpty() || title.isEmpty() || chassisNumber.isEmpty() || engineNumber.isEmpty() ||
                registrationNumber.isEmpty() || image == null) {
            throw new InvalidPropertiesFormatException((String) null);
        }
        if (!vehicleType.endsWith("Motorcycle") && number.isEmpty()) {
            throw new InvalidPropertiesFormatException((String) null);
        }
    }
}
